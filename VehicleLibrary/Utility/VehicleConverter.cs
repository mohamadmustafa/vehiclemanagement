﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using VehicleLibrary.Models.Bike;
using VehicleLibrary.Models.Car;
using VehicleLibrary.Models.Vehicle;

namespace VehicleLibrary.Utility
{
    public static class Utils
    {
        public static IVehicle ConvertVehicle(this IVehicle vehicle)
        {
            if (vehicle == null) return null;

            var convertedVehicle = vehicle.VehicleType switch
            {
                VehicleType.Car => new Car(vehicle),
                VehicleType.Bike => new Bike(vehicle),
                _ => default(IVehicle)
            };

            return convertedVehicle;
        }
    }

    public class VehicleConverter : JsonConverter
    {
        public override bool CanWrite => false;
        public override bool CanRead => true;
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(IVehicle);
        }
        public override void WriteJson(JsonWriter writer,
            object value, JsonSerializer serializer)
        {
            throw new InvalidOperationException("Use default serialization.");
        }

        public override object ReadJson(JsonReader reader,
            Type objectType, object existingValue,
            JsonSerializer serializer)
        {
            var jsonObject = JObject.Load(reader);

            var vehicleTypeValue =
                (jsonObject["vehicleType"] ?? throw new Exception("Vehicle object must have a valid type."))
                .Value<string>();

            var vehicleType = Enum.Parse(typeof(VehicleType), vehicleTypeValue);
            var vehicle = vehicleType switch
            {
                VehicleType.Car => new Car(),
                VehicleType.Bike => new Bike(),
                _ => default(IVehicle)
            };
            serializer.Populate(jsonObject.CreateReader(),
                vehicle ?? throw new Exception("Error in deserialization or Vehicle type is not supported."));
            return vehicle;
        }
    }
}