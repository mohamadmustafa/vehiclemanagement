﻿using System;
using Newtonsoft.Json;
using VehicleLibrary.Models.Vehicle;

namespace VehicleLibrary.Models.Car
{
    public class Car : IVehicle
    {
        public Car() { }

        public Car(IVehicle vehicle)
        {
            Id = vehicle.Id;
            Make = vehicle.Make;
            Model = vehicle.Model;
            Price = vehicle.Price;
            Properties = vehicle.Properties;
            DateCreated = vehicle.DateCreated;
        }

        public Guid? Id { get; set; }
        public VehicleType VehicleType => VehicleType.Car;
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public string Properties
        {
            get => JsonConvert.SerializeObject(CarProperties);
            set => CarProperties = JsonConvert.DeserializeObject<CarProperties>(value);
        }
        public DateTime? DateCreated { get; set; }
        public CarProperties CarProperties { get; set; }
    }

    public class CarValidator : VehicleValidator<Car>
    {
        public CarValidator() { }
    }
}