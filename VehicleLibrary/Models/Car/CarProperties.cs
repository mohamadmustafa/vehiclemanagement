﻿namespace VehicleLibrary.Models.Car
{
    public class CarProperties
    {
        public int Doors { get; set; }
        public BodyType BodyType { get; set; }
    }
}