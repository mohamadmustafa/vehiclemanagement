﻿namespace VehicleLibrary.Models.Car
{
    public enum BodyType
    {
        Sedan,
        Hatchback,
        Coupe,
        Wagon,
        Convertible,
        Van,
        Jeep,
        PickUp,
        SUV,
        Ute
    }
}