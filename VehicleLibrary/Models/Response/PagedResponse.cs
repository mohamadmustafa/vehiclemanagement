﻿using System.Collections.Generic;
using VehicleLibrary.Models.Vehicle;

namespace VehicleLibrary.Models.Response
{
    public class PagedResponse<T> where T : IVehicle
    {
        public List<T> Data { get; set; }
        public int TotalCount { get; set; }
        public VehicleOrderBy OrderBy { get; set; }
        public SortOrder SortOrder { get; set; }
        public int PageNumber { get; set; }
        public int? PageSize { get; set; }
        public bool? HasMoreRecords { get; set; }
    }
}