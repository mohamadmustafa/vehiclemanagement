﻿using System;
using Newtonsoft.Json;
using VehicleLibrary.Models.Vehicle;

namespace VehicleLibrary.Models.Bike
{
    public class Bike : IVehicle
    {
        public Bike() { }

        public Bike(IVehicle vehicle)
        {
            Id = vehicle.Id;
            Make = vehicle.Make;
            Model = vehicle.Model;
            Price = vehicle.Price;
            Properties = vehicle.Properties;
            DateCreated = vehicle.DateCreated;
        }

        public Guid? Id { get; set; }
        public VehicleType VehicleType => VehicleType.Bike;
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal Price { get; set; }
        public string Properties
        {
            get => JsonConvert.SerializeObject(BikeProperties);
            set => BikeProperties = JsonConvert.DeserializeObject<BikeProperties>(value);
        }
        public DateTime? DateCreated { get; set; }
        public BikeProperties BikeProperties { get; set; }
    }
}