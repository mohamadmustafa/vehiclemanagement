﻿using FluentValidation;

namespace VehicleLibrary.Models.Vehicle
{
    public class VehicleSearch
    {
        public VehicleSearch()
        {
            OrderBy = VehicleOrderBy.DateCreated;
            SortOrder = SortOrder.Desc;
            PageNumber = 1;
            PageSize = 10;
        }

        public VehicleType? VehicleType;
        public string Make { get; set; }
        public string Model { get; set; }
        public decimal? MaxPrice { get; set; }
        public decimal? MinPrice { get; set; }
        public VehicleOrderBy OrderBy { get; set; }
        public SortOrder SortOrder { get; set; }
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
    }

    public enum VehicleOrderBy
    {
        VehicleType,
        Make,
        Model,
        Price,
        DateCreated
    }

    public enum SortOrder
    {
        Asc,
        Desc
    }

    public class VehicleSearchValidator : AbstractValidator<VehicleSearch>
    {
        public VehicleSearchValidator()
        {
            const int pageSizeLimit = 100;
            RuleFor(m => m.PageNumber).NotNull().GreaterThan(0).LessThan(int.MaxValue);
            RuleFor(m => m.PageSize).NotNull().GreaterThan(-1).LessThanOrEqualTo(pageSizeLimit);
        }
    }
}