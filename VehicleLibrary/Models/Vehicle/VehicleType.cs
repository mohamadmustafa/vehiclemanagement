﻿namespace VehicleLibrary.Models.Vehicle
{
    public enum VehicleType
    {
        Car,
        Boat,
        Bike,
        Caravan,
        Truck
    }
}