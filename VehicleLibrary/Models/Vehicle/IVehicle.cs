﻿using System;
using FluentValidation;
using Newtonsoft.Json;

namespace VehicleLibrary.Models.Vehicle
{
    public interface IVehicle
    {
        [JsonRequired]
        Guid? Id { get; set; }

        [JsonRequired]
        VehicleType VehicleType { get; }

        [JsonRequired]
        string Make { get; set; }

        [JsonRequired]
        string Model { get; set; }
        
        [JsonRequired]
        decimal Price { get; set; }

        [JsonIgnore]
        string Properties { get; set; }

        DateTime? DateCreated { get; set; }
    }

    public abstract class VehicleValidator<T> : AbstractValidator<T> where T : IVehicle
    {
        protected VehicleValidator()
        {
            RuleFor(m => m.Id).NotNull();
            RuleFor(m => m.VehicleType).NotNull();
            RuleFor(m => m.Make).NotNull().NotEmpty().Length(1, 100);
            RuleFor(m => m.Model).NotNull().NotEmpty().Length(1, 100);
            RuleFor(m => m.Price).NotNull().NotEmpty().GreaterThan(0);
        }
    }
}