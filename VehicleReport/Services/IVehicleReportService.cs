﻿using System.Threading.Tasks;

namespace VehicleReport.Services
{
    public interface IVehicleReportService
    {
        Task GenerateReport();
    }
}
