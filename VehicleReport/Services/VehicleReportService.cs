﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading.Tasks;
using VehicleLibrary.Models.Vehicle;
using VehicleReport.Repositories;

namespace VehicleReport.Services
{
    public class VehicleReportService : IVehicleReportService
    {
        private readonly IVehicleClient _vehicleClient;
        private const string FormatterString = "-------------------------------------------------------------\n";
        private const string DateTimeFormat = "dd-MM-yyyy hh-mm-ss";
        private const string IdColumn = "Id";
        private const string VehicleTypeColumn = "Vehicle Type";
        private const string MakeColumn = "Make";
        private const string ModelColumn = "Model";
        private const string PriceColumn = "Price";

        public VehicleReportService(IVehicleClient vehicleClient)
        {
            _vehicleClient = vehicleClient;
        }

        public async Task GenerateReport()
        {
            // Default query to get all vehicles types
            var query = new VehicleSearch();
            var vehicles = await _vehicleClient.SearchVehicles(query);

            // Format report
            var date = DateTime.UtcNow.ToString(DateTimeFormat);
            var header =
                FormatterString + $"Vehicle Report {date}\n" + FormatterString;

            var totalVehicleNumber = $"Total Vehicle Count: {vehicles.TotalCount}\n";

            var lastCreatedVehicles = GetLastCreatedVehicles(vehicles.Data);

            string[] content = {
                header,
                totalVehicleNumber,
                lastCreatedVehicles
            };

            // Write to file
            var file = new System.IO.FileInfo($"C:\\Logs\\Report\\Vehicle Report {date}.txt");
            file.Directory?.Create();
            await System.IO.File.WriteAllLinesAsync(file.FullName, content);
        }

        public static string GetLastCreatedVehicles(IEnumerable<IVehicle> vehicles)
        {
            var lastCreatedVehicles = "Last 10 Created Vehicles:\n\n" +
                AddSpaces(IdColumn) + AddSpaces(VehicleTypeColumn) + AddSpaces(MakeColumn) + AddSpaces(ModelColumn) +
                AddSpaces(PriceColumn) + "\n";

            if (vehicles == null) return lastCreatedVehicles;

            foreach (var vehicle in vehicles)
            {
                lastCreatedVehicles +=
                    AddSpaces(vehicle.Id.ToString()) + AddSpaces(vehicle.VehicleType.ToString()) +
                    AddSpaces(vehicle.Make) +
                    AddSpaces(vehicle.Model) + AddSpaces(vehicle.Price.ToString(CultureInfo.InvariantCulture)) +
                    "\n";
            }

            return lastCreatedVehicles;
        }

        public static string AddSpaces(string column)
        {
            const int spaceNumber = 50;
            const string space = " ";
            var columnWithSpaces = column;
            for (var i = column.Length; i < spaceNumber; i++)
            {
                columnWithSpaces += space;
            }

            return columnWithSpaces;
        }
    }
}