﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Quartz.Spi;
using Topshelf;
using VehicleReport.BackgroundJobs;
using VehicleReport.Repositories;
using VehicleReport.Services;

namespace VehicleReport
{
    class Program
    {
        private static IServiceProvider _serviceProvider;

        static void Main(string[] args)
        {
            RegisterServices();
            IServiceScope scope = _serviceProvider.CreateScope();

            // Setup a scheduled job to run every hour
            HostFactory.Run(configure =>
            {
                configure.Service<BackgroundService>(service =>
                {
                    var jobFactory = scope.ServiceProvider.GetRequiredService<IJobFactory>();
                    service.ConstructUsing(s => new BackgroundService(jobFactory));
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                //Setup Account that window service use to run.  
                configure.RunAsLocalSystem();
                configure.SetServiceName("VehicleReportService");
                configure.SetDisplayName("Vehicle Report Service");
                configure.SetDescription("Vehicle Report Service");
            });

            DisposeServices();
        }

        private static void RegisterServices()
        {
            var services = new ServiceCollection();

            services.AddHttpClient("vehicle", c => c.BaseAddress = new Uri("https://localhost:44372"));
            services.AddSingleton<IVehicleClient, VehicleClient>();
            services.AddScoped<IVehicleReportService, VehicleReportService>();

            services.AddSingleton<IJobFactory>(provider =>
            {
                var jobFactory = new JobFactory(provider);
                return jobFactory;
            });
            services.AddSingleton<VehicleReportJob>();

            _serviceProvider = services.BuildServiceProvider();
        }

        private static void DisposeServices()
        {
            if (_serviceProvider == null) return;
            if (_serviceProvider is IDisposable disposable)
            {
                disposable.Dispose();
            }
        }
    }
}