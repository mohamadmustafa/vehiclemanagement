﻿using System.Threading.Tasks;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;

namespace VehicleReport.Repositories
{
    public interface IVehicleClient
    {
        Task<PagedResponse<IVehicle>> SearchVehicles(VehicleSearch query);
    }
}
