﻿using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;
using VehicleLibrary.Utility;

namespace VehicleReport.Repositories
{
    public class VehicleClient : IVehicleClient
    {
        private readonly IHttpClientFactory _factory;
        private const string VehicleSearchUrl = "/Vehicle/Search";
        public VehicleClient(IHttpClientFactory factory)
        {
            _factory = factory;
        }

        public async Task<PagedResponse<IVehicle>> SearchVehicles(VehicleSearch query)
        {
            var client = _factory.CreateClient("vehicle");
            var bodyRequest = JsonConvert.SerializeObject(query);
            var requestContent = new StringContent(bodyRequest, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(VehicleSearchUrl, requestContent);
            var responseContent = await response.Content.ReadAsStringAsync();
            var result = JsonConvert.DeserializeObject<PagedResponse<IVehicle>>(responseContent, new VehicleConverter());
            return result;
        }
    }
}