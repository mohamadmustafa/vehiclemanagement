﻿using System;
using Quartz;
using Quartz.Impl;
using Quartz.Spi;
using VehicleReport.BackgroundJobs;

namespace VehicleReport
{
    public class BackgroundService
    {
        private readonly IJobFactory _jobFactory;

        public BackgroundService(IJobFactory jobFactory)
        {
            _jobFactory = jobFactory;
        }

        public void Start()
        {
            try
            {
                var schedulerFactory = new StdSchedulerFactory();
                var scheduler = schedulerFactory.GetScheduler().Result;
                scheduler.JobFactory = _jobFactory;
                scheduler.Start().Wait();

                var jobDetails = JobBuilder.Create<VehicleReportJob>()
                    .WithIdentity(JobKey.Create("Vehicle Report Processing", "Hourly Report Processing"))
                    .Build();

                var trigger = TriggerBuilder.Create()
                    .WithIdentity(new TriggerKey("Generate Hourly Vehicle Report", "Hourly Report Processing"))
                    .StartNow()
                    .WithSimpleSchedule(builder =>
                    { 
                        builder.WithIntervalInHours(1)
                            .RepeatForever();
                    })
                    .Build();

                scheduler.ScheduleJob(jobDetails, trigger).Wait();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Stop()
        {
            // write code here that runs when the Windows Service stops.
        }
    }
}
