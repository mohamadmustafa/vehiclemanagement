﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Moq;
using VehicleLibrary.Models.Car;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;
using VehicleManagement.Controllers;
using VehicleManagement.Repositories;
using Xunit;

namespace VehicleManagementUnitTests.Controllers
{
    public class VehicleControllerTest
    {
        private readonly Mock<IVehicleRepository> _mockVehicleRepository;
        private readonly VehicleController _mockVehicleController;

        public VehicleControllerTest()
        {
            _mockVehicleRepository = new Mock<IVehicleRepository>();
            _mockVehicleController = new VehicleController(_mockVehicleRepository.Object);
        }

        [Fact]
        public async void GetCarById_ValidRequest_ShouldWork_ReturnsOk()
        {
            // Arrange
            IVehicle expectedCarResult = new Car
            {
                Id = Guid.NewGuid(),
                Make = "Ford",
                Model = "Focus",
                Price = 10000,
                DateCreated = DateTime.UtcNow,
                CarProperties = new CarProperties
                {
                    Doors = 4,
                    BodyType = BodyType.Hatchback
                }
            };

            _mockVehicleRepository.Setup(repo => repo.GetVehicleById(It.IsAny<Guid>())).Returns(Task.FromResult(expectedCarResult));

            // Act
            var result = await _mockVehicleController.GetVehicleById(expectedCarResult.Id.Value);
            var okResult = result as OkObjectResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
            var actualCarResult = okResult.Value;
            Assert.NotNull(actualCarResult);
            Assert.Equal(expectedCarResult, actualCarResult);
        }

        [Fact]
        public async void SearchVehicleList_GetAllVehicleTypes_ShouldWork_ReturnsOk()
        {
            // Arrange

            var expectedVehicleListResult = new PagedResponse<IVehicle>
            {
                HasMoreRecords = false,
                PageSize = 10,
                PageNumber = 1,
                TotalCount = 20,
                OrderBy = VehicleOrderBy.DateCreated,
                SortOrder = SortOrder.Desc,
                Data = new List<IVehicle>
                {
                    new Car
                    {
                        Id = Guid.NewGuid(),
                        Make = "Ford",
                        Model = "Focus",
                        Price = 10000,
                        DateCreated = DateTime.UtcNow,
                        CarProperties = new CarProperties
                        {
                            Doors = 4,
                            BodyType = BodyType.Hatchback
                        }
                    }
                }
            };

            // Default search query
            var query = new VehicleSearch();

            _mockVehicleRepository.Setup(repo => repo.GetVehicleList(It.IsAny<VehicleSearch>()))
                .Returns(Task.FromResult(expectedVehicleListResult));

            // Act
            var result = await _mockVehicleController.SearchVehicleList(query);
            var okResult = result as OkObjectResult;

            // Assert
            Assert.NotNull(okResult);
            Assert.Equal(200, okResult.StatusCode);
            var actualCarResult = okResult.Value;
            Assert.NotNull(actualCarResult);
            Assert.Equal(expectedVehicleListResult, actualCarResult);
        }

        [Fact]
        public async void VehicleSearch_InvalidPageParameters_ShouldBeInvalid()
        {
            // Arrange
            var vehicleSearchValidator = new VehicleSearchValidator();
            var query = new VehicleSearch{PageNumber = -1, PageSize = -5};

            // Act
            var queryIsValid = (await vehicleSearchValidator.ValidateAsync(query)).IsValid;

            // Assert
            Assert.False(queryIsValid);
        }
    }
}
