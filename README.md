# Vehicle Management
This project includes a Vehicle Management API along with a background service to run hourly reports using the API.

Below is a brief list of principles that were used:

1. Used in-memory SQL Server DB. NOTE: Please modify the `VehicleDbConnectionString` in the appsettings file to match your machine's absolute path, as in-memory SQL Server DB has issues when settings it to a relative path.
2. Applied Repository design pattern with DI to enable loose coupling between components.
3. Adhered more to the DRY principle by using inheritance and abstraction when possible, as well as efficient code structure.
4. Added global exception handling using Middlewares.
5. As comprehensive as possible input and data validation.
6. Used Dapper as a helper library for running queries.
7. Used a connection factory to manage DB connections.
8. Configured Serilog for error and information logging. When running the project logs can be found in `C:\\Logs\\Development\\Vehicle Management API-.txt`
9. Configured Swagger for API documentation. It can be accessed through `https://localhost:44372/` when running the project.
10. Vehicle list end point powered with searching capabilities as well as pagination and sorting.
11. Used Topshelf and Quartz for creating the windows service with .Net's DI container. Please set up the project as a startup project or run `VehicleReport.exe install` command to install service. Reports can be found in `C:\Logs\Report`.
12. And finally, Unit testing as well as a Postman test collection that was handy during testing. The collection resides within the folder `vehiclemanagement\VehicleManagement\Tests\IntegtrationTests`.

Doing this coding assessment was enjoyable. I am looking forward to discussing the solution in more detail in person.

# Mohamad Mustafa
