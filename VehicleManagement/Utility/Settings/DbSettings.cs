﻿namespace VehicleManagement.Utility.Settings
{
    public class DbSettings
    {
        public string VehicleDbConnectionString { get; set; }
    }
}