﻿using System.Collections.Generic;
using System.Linq;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;

namespace VehicleManagement.Utility
{
    public static class QueryHelper
    {
        public static string ToMultipleResultsPagedQuery(this string query, string orderBy, string sortOrder, int pageNumber, int pageSize)
        {
            var multipleResultsPagedQuery =
                $@"{query}
                ORDER BY {orderBy} {sortOrder} OFFSET {(pageNumber - 1) * pageSize} ROWS FETCH NEXT {pageSize} ROWS ONLY;
                SELECT COUNT(*) AS TotalNumberCount FROM ({query}) AS RecordList;";

            return multipleResultsPagedQuery;
        }

        public static PagedResponse<T> PagedResponse<T>(this IEnumerable<T> results, int totalCount, VehicleOrderBy orderBy,
            SortOrder sortOrder, int pageNumber, int pageSize) where T : IVehicle
        {
            var enumerable = results.ToList();
            return new PagedResponse<T>
            {
                Data = enumerable,
                TotalCount = totalCount > 0 ? totalCount : 0,
                PageNumber = pageNumber,
                PageSize = pageSize,
                OrderBy = orderBy,
                SortOrder = sortOrder,
                HasMoreRecords = pageNumber * pageSize < (totalCount > 0 ? totalCount : 0)
            };
        }

        public static string ToLookup(this string property)
        {
            return !string.IsNullOrEmpty(property) ? $"%{property}%" : property;
        }
    }
}