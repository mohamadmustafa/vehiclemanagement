﻿using System.Transactions;

namespace VehicleManagement.Repositories
{
    public interface IBaseRepository
    {
        TransactionScope CreateTransactionScope(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);
    }
}