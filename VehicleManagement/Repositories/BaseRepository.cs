﻿using System.Data;
using System.Transactions;
using VehicleManagement.Database;
using IsolationLevel = System.Transactions.IsolationLevel;

namespace VehicleManagement.Repositories
{
    public class BaseRepository : IBaseRepository
    {
        public IDbConnection VehicleDbConnection { get; }

        public BaseRepository(IDbConnectionFactory dbConnectionFactory)
        {
            VehicleDbConnection = dbConnectionFactory.CreateVehicleDbConnection();
        }

        public TransactionScope CreateTransactionScope(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            var transactionOptions = new TransactionOptions
            {
                IsolationLevel = isolationLevel,
                Timeout = TransactionManager.MaximumTimeout
            };

            return new TransactionScope(TransactionScopeOption.Required, transactionOptions,
                TransactionScopeAsyncFlowOption.Enabled);
        }
    }
}