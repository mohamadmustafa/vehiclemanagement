﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Serilog;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;
using VehicleLibrary.Utility;
using VehicleManagement.Database;
using VehicleManagement.Utility;

namespace VehicleManagement.Repositories
{
    public class VehicleRepository : BaseRepository, IVehicleRepository
    {
        public VehicleRepository(IDbConnectionFactory dbConnectionFactory) : base(dbConnectionFactory)
        {
        }

        public async Task<IVehicle> GetVehicleById(Guid? vehicleId)
        {
            if (vehicleId == null) return null;

            Log.Information($"Getting Vehicle with Id {vehicleId}");

            const string sqlQuery =
                @"SELECT Id, VehicleType, Make, Model, Price, Properties, DateCreated
                FROM Vehicle
                WHERE Id = @Id;";

            var result = await VehicleDbConnection.QueryFirstOrDefaultAsync<Vehicle>(sqlQuery,
                new
                {
                    Id = vehicleId.ToString()
                });

            return result.ConvertVehicle();
        }

        public async Task<PagedResponse<IVehicle>> GetVehicleList(VehicleSearch query)
        {
            const string sqlQuery =
                @"SELECT Id, VehicleType, Make, Model, Price, Properties, DateCreated
                FROM Vehicle
                WHERE ((@VehicleType IS NULL OR @VehicleType = '') OR (VehicleType LIKE @VehicleType))
                AND ((@Make IS NULL OR @Make = '') OR (Make LIKE @Make))
                AND ((@Model IS NULL OR @Model = '') OR (Model LIKE @Model))
                AND ((@MaxPrice IS NULL) OR (Price <= @MaxPrice))
                AND ((@MinPrice IS NULL) OR (Price >= @MinPrice))";

            var multiResultSet = await VehicleDbConnection.QueryMultipleAsync(
                sqlQuery.ToMultipleResultsPagedQuery(query.OrderBy.ToString(), query.SortOrder.ToString(), query.PageNumber, query.PageSize),
                new
                {
                    VehicleType = query.VehicleType.ToString().ToLookup(),
                    Make = query.Make.ToLookup(),
                    Model = query.Model.ToLookup(),
                    query.MaxPrice,
                    query.MinPrice
                });

            // Gets the vehicle list form the multi result query first
            var vehicleList = multiResultSet.Read<Vehicle>().Select(vehicle => vehicle.ConvertVehicle());

            // Gets the total count of the vehicle list as a separate query
            var vehicleListCount = multiResultSet.Read<int>().First();

            return vehicleList.PagedResponse(vehicleListCount, query.OrderBy, query.SortOrder,
                query.PageNumber, query.PageSize);
        }

        public async Task<IVehicle> CreateVehicle(IVehicle vehicle)
        {
            if (vehicle.Id == null) return null;

            Log.Information($"Creating Vehicle with Id {vehicle.Id}");

            const string sqlQuery =
                @"INSERT INTO Vehicle (Id, VehicleType, Make, Model, Price, Properties, DateCreated)
                VALUES (@Id, @VehicleType, @Make, @Model, @Price, @Properties, @DateCreated);";

            await VehicleDbConnection.ExecuteAsync(sqlQuery,
                new
                {
                    Id = vehicle.Id.ToString(),
                    VehicleType = vehicle.VehicleType.ToString(),
                    vehicle.Make,
                    vehicle.Model,
                    vehicle.Price,
                    vehicle.Properties,
                    DateCreated = DateTime.UtcNow
                });

            var newVehicle = await GetVehicleById(vehicle.Id);
            return newVehicle;

        }

        public async Task<IVehicle> UpdateVehicle(Guid? vehicleId, IVehicle vehicle)
        {
            if (vehicleId == null) return null;

            Log.Information($"Updating Vehicle with Id {vehicle.Id}");

            const string sqlQuery =
                @"UPDATE Vehicle
                SET VehicleType = @VehicleType, Make = @Make, Model = @Model, Price = @Price, Properties = @Properties
                WHERE Id = @Id;";

            await VehicleDbConnection.ExecuteAsync(sqlQuery,
                new
                {
                    Id = vehicleId,
                    VehicleType = vehicle.VehicleType.ToString(),
                    vehicle.Make,
                    vehicle.Model,
                    vehicle.Price,
                    vehicle.Properties
                });

            var updatedVehicle = await GetVehicleById(vehicleId);
            return updatedVehicle;
        }

        public async Task<bool> DeleteVehicle(Guid? vehicleId)
        {
            if (vehicleId == null) return true;

            Log.Information($"Deleting Vehicle with Id {vehicleId}");

            const string sqlQuery =
                @"DELETE FROM Vehicle
                WHERE Id = @Id;";

            var affectedRows = await VehicleDbConnection.ExecuteAsync(sqlQuery,
                new
                {
                    Id = vehicleId.ToString(),
                });

            return affectedRows > -1;
        }

        // A private concrete Vehicle class used for querying
        private class Vehicle : IVehicle
        {
            public Guid? Id { get; set; }
            public VehicleType VehicleType { get; }
            public string Make { get; set; }
            public string Model { get; set; }
            public decimal Price { get; set; }
            public string Properties { get; set; }
            public DateTime? DateCreated { get; set; }
        }
    }
}