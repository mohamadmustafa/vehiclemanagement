﻿using System;
using System.Threading.Tasks;
using VehicleLibrary.Models.Response;
using VehicleLibrary.Models.Vehicle;

namespace VehicleManagement.Repositories
{
    public interface IVehicleRepository : IBaseRepository
    {
        public Task<IVehicle> GetVehicleById(Guid? vehicleId);
        public Task<PagedResponse<IVehicle>> GetVehicleList(VehicleSearch query);
        public Task<IVehicle> CreateVehicle(IVehicle vehicle);
        public Task<IVehicle> UpdateVehicle(Guid? vehicleId, IVehicle vehicle);
        public Task<bool> DeleteVehicle(Guid? vehicleId);
    }
}
