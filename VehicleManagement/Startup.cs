using System.Text.Json.Serialization;
using FluentValidation;
using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using VehicleLibrary.Models.Car;
using VehicleLibrary.Models.Vehicle;
using VehicleLibrary.Utility;
using VehicleManagement.Database;
using VehicleManagement.Middleware;
using VehicleManagement.Repositories;
using VehicleManagement.Utility.Settings;

namespace VehicleManagement
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc()
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver.ResolveContract(typeof(IVehicle)).Converter =
                        new VehicleConverter();
                    options.SerializerSettings.Converters.Add(new StringEnumConverter());
                })
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                })
                .AddFluentValidation();

            services.Configure<DbSettings>(Configuration.GetSection("DatabaseSettings"));

            services.AddTransient<IDbConnectionFactory, DbConnectionFactory>();

            // Register validators
            services.AddTransient<IValidator<Car>, CarValidator>();
            services.AddTransient<IValidator<VehicleSearch>, VehicleSearchValidator>();
            
            // Register repositories and services
            services.AddScoped<IBaseRepository, BaseRepository>();
            services.AddScoped<IVehicleRepository, VehicleRepository>();

            services.AddControllers();

            // Register the Swagger generator
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1.0", new OpenApiInfo
                {
                    Version = "v1.0",
                    Title = "Vehicle Management API",
                    Description = "Vehicle Management API. Built with ASP.Net Core 3.1.",
                    Contact = new OpenApiContact
                    {
                        Name = "Mohamad Mustafa",
                        Email = "mohamadmu95@gmail.com"
                    }
                });
            });

            services.AddSwaggerGenNewtonsoftSupport();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseGlobalExceptions();
            }
            else
            {
                // For more useful exceptions when consuming the API
                app.UseGlobalExceptions();
            }

            // Enable middleware for Swagger
            app.UseSwagger();
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("./swagger/v1.0/swagger.json", "Vehicle Management API");
                options.EnableFilter();
                options.RoutePrefix = string.Empty;
            });

            app.UseHttpsRedirection();

            app.UseRouting();
 
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}