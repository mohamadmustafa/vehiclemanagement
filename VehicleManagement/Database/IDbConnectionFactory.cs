﻿using System.Data;

namespace VehicleManagement.Database
{
    public interface IDbConnectionFactory
    {
        IDbConnection CreateVehicleDbConnection();
    }
}