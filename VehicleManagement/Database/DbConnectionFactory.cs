﻿using System.Data;
using System.Data.SqlClient;
using Microsoft.Extensions.Options;
using VehicleManagement.Utility.Settings;

namespace VehicleManagement.Database
{
    public class DbConnectionFactory : IDbConnectionFactory
    {
        private readonly IOptions<DbSettings> _dbSettings;

        public DbConnectionFactory(IOptions<DbSettings> dbSettings)
        {
            _dbSettings = dbSettings;
        }

        public IDbConnection CreateVehicleDbConnection()
        {
            return CreateDbConnection(_dbSettings.Value.VehicleDbConnectionString);
        }

        private IDbConnection CreateDbConnection(string dbConnectionString)
        {
            return new SqlConnection(dbConnectionString);
        }
    }
}