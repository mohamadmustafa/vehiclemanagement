﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using VehicleLibrary.Models.Vehicle;
using VehicleManagement.Repositories;

namespace VehicleManagement.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VehicleController : ControllerBase
    {
        private readonly IVehicleRepository _vehicleRepository;

        public VehicleController(IVehicleRepository vehicleRepository)
        {
            _vehicleRepository = vehicleRepository;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Welcome to Vehicle Management API!");
        }

        /// <summary>
        /// Gets a specific Vehicle by id.
        /// </summary>
        /// <param name="vehicleId">The Id of the Vehicle.</param>
        /// <returns>A single Vehicle object.</returns>
        /// <response code="200">Success</response>
        /// <response code="404">Not Found</response>
        /// <response code="500">Internal Server Error - Something went wrong</response>
        [Route("{vehicleId}", Name = "GetVehicleById")]
        [HttpGet]
        public async Task<IActionResult> GetVehicleById([FromRoute] Guid vehicleId)
        {
            var vehicle = await _vehicleRepository.GetVehicleById(vehicleId);
            if (vehicle == null) return NotFound($"Vehicle with Id {vehicleId} was not found");

            return Ok(vehicle);
        }

        /// <summary>
        /// Gets a list of Vehicles in a paginated fashion.
        /// The Vehicle list is wrapped around an object which provides useful pagination information.
        /// </summary>
        /// <param name="query">Provides search capabilities for Vehicles list.</param>
        /// <returns>A list of paginated Vehicles.</returns>
        /// <response code="200">Success</response>
        /// <response code="500">Internal Server Error - Something went wrong</response>
        [Route("Search", Name = "SearchVehicleList")]
        [HttpPost]
        public async Task<IActionResult> SearchVehicleList([FromBody] VehicleSearch query)
        {
            var list = await _vehicleRepository.GetVehicleList(query);
            return Ok(list);
        }

        /// <summary>
        /// Creates a new Vehicle into the DB if there is not already a Vehicle with the same Id.
        /// </summary>
        /// <param name="vehicle">The Vehicle object that is to be created.</param>
        /// <returns>The newly created Vehicle object along with the GET link to fetch the object in the headers.</returns>
        /// <response code="201">Created</response>
        /// <response code="400">Bad Request</response>
        /// <response code="500">Internal Server Error - Something went wrong</response>
        [HttpPost]
        public async Task<IActionResult> CreateVehicle([FromBody] IVehicle vehicle)
        {
            // Check if vehicle already exists
            var existingVehicle = await _vehicleRepository.GetVehicleById(vehicle.Id);

            if (existingVehicle != null)
                return BadRequest($"Cannot create new Vehicle. Vehicle with Id {vehicle.Id} already exists.");

            var newVehicle = await _vehicleRepository.CreateVehicle(vehicle);
            return CreatedAtRoute("GetVehicleById", new { vehicleId = newVehicle.Id }, newVehicle);
        }

        /// <summary>
        /// Updates a Vehicle in the DB or inserts it if it does not exists.
        /// </summary>
        /// <param name="vehicleId">Id of the Vehicle that is to be updated.</param>
        /// <param name="vehicle">The Vehicle object that is to be updated.</param>
        /// <returns>The updated or created Vehicle object.</returns>
        /// <response code="201">Created</response>
        /// <response code="202">Accepted</response>
        /// <response code="500">Internal Server Error - Something went wrong</response>
        [Route("{vehicleId}")]
        [HttpPut]
        public async Task<IActionResult> UpdateVehicle([FromRoute] Guid vehicleId, [FromBody] IVehicle vehicle)
        {
            // Check if vehicle exists first and insert it if it does not
            var existingVehicle = await _vehicleRepository.GetVehicleById(vehicleId);

            if (existingVehicle == null)
            {
                Log.Information(
                    $"Vehicle with Id {vehicleId} does not exists. New vehicle will be created instead of updating it.");
                var newVehicle = await _vehicleRepository.CreateVehicle(vehicle);
                return CreatedAtRoute("GetVehicleById", new { vehicleId = newVehicle.Id }, newVehicle);
            }

            var updatedVehicle = await _vehicleRepository.UpdateVehicle(vehicleId, vehicle);
            return AcceptedAtRoute("GetVehicleById", new { vehicleId = updatedVehicle.Id }, updatedVehicle);
        }


        /// <summary>
        /// Deletes a Vehicle from the DB.
        /// </summary>
        /// <param name="vehicleId">Id of the Vehicle that is to be deleted</param>
        /// <returns>Status code 202 if successful.</returns>
        /// <response code="202">Accepted</response>
        /// <response code="500">Internal Server Error - Something went wrong</response>
        [Route("{vehicleId}")]
        [HttpDelete]
        public async Task<IActionResult> DeleteVehicle([FromRoute] Guid vehicleId)
        {
            var deleteResult = await _vehicleRepository.DeleteVehicle(vehicleId);

            if (deleteResult)
                return Accepted();

            throw new Exception($"Deleting Vehicle was unsuccessful or Vehicle with Id {vehicleId} does not exist.");
        }
    }
}